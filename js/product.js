function fetchCoinStats(coin, miners) {
  var coinPrice = 0

  $('#coin-stat-symbol').text(coin)

  fetch('https://api.nomics.com/v1/prices?key=708024871f68e219c1d4d606bc67042a')
    .then(function(response) {
      return response.json()
    })
    .then(function(data) {
      for (var index = 0; index < data.length; index++) {
        // console.log('index=' + index + ': ' + data[index].price)
        if (data[index].currency === coin) {
          coinPrice = parseFloat(data[index].price)
          var minFrac
          if (coinPrice < 0.1) {
            minFrac = 5
          }
          $('#coin-stat-price').text(formatDollars(coinPrice))
          break
        }
      }

      fetch('https://api.nomics.com/v1/dashboard?key=708024871f68e219c1d4d606bc67042a')
        .then(function(response) {
          return response.json()
        })
        .then(function(data) {
          for (var index = 0; index < data.length; index++) {
            // console.log('index=' + index + ': ' + data[index].price)
            if (data[index].currency === coin) {
              var supply = parseFloat(data[index].availableSupply)
              $('#coin-stat-supply').text(formatNumber(supply))

              var marketCap = supply * coinPrice
              $('#coin-stat-marketcap').text(formatDollars(Math.round(marketCap)))
              break
            }
          }
        })

      fetch('https://api.nomics.com/v1/dashboard?key=708024871f68e219c1d4d606bc67042a')
        .then(function(response) {
          return response.json()
        })
        .then(function(data) {
          for (var index = 0; index < data.length; index++) {
            // console.log('index=' + index + ': ' + data[index].price)
            if (data[index].currency === coin) {
              var volume = parseFloat(data[index].dayVolume)
              $('#coin-stat-volume').text(formatNumber(Math.round(volume)))

              var dayOpen = parseFloat(data[index].dayOpen)
              var percentChange = (coinPrice / dayOpen) * 100 - 100

              $('#coin-stat-percent-change').text(
                (percentChange < 0 ? '' : '+') + percentChange.toFixed(2) + '%',
              )
              $('#coin-stat-percent-change').addClass(
                percentChange < 0 ? 'coin-stat-negative' : 'coin-stat-positive',
              )
              break
            }
          }
        })
    })

  $.getJSON(
    'https://api.allorigins.ml/get?url=' +
      encodeURIComponent('https://www.apanel.com/') +
      '&callback=?',
    function(data) {
      try {
        // console.log(data.contents)
        var domNodes = $($.parseHTML(data.contents))
        var rows = domNodes.find('#miners-table > tbody > tr')

        for (var m = 0; m < miners.length; m++) {
          var miner = miners[m]
          for (var i = 0; i < rows.length; i++) {
            var row = rows[i]
            var dataUrl = $(row).attr('data-url')
            if (dataUrl.indexOf(miner.path) > 0) {
              lastChild = $(row).find('td:last-child')

              var parts = lastChild
                .text()
                .trim()
                .split(' / ')

              var dailyProfit = parseFloat(parts[0].slice(1))
              if (miner.multiplier) {
                dailyProfit = dailyProfit * miner.multiplier
              }
              $('#' + miner.id).text(formatDollars(Math.round(dailyProfit * 100) / 100))
              $('#' + miner.id).addClass(
                dailyProfit < 0 ? 'coin-stat-negative' : 'coin-stat-positive',
              )
            }
          }
        }
      } catch (e) {
        console.log('Unable to fetch or parse daily mining profits')
      }
    },
  )
}

function fetchOrderCounts(models) {
  fetch('https://portal.obelisk.tech/api/orderCounts')
    .then(function(response) {
      return response.json()
    })
    .then(function(data) {
      for (var i = 0; i < models.length; i++) {
        var modelInfo = models[i]
        var numSold = data.counts[modelInfo.model]
        if (numSold) {
          $('#' + modelInfo.countId).text(formatNumber(numSold))
        }

        var totalAvail = data.available[modelInfo.model]
        if (totalAvail) {
          $('#' + modelInfo.availId).text(formatNumber(totalAvail))
        }

        // Update progress bar
        if (modelInfo.progressBarId && numSold > 0 && totalAvail > 0) {
          var percent = (percent = (numSold / totalAvail) * 100)
          $('#' + modelInfo.progressBarId).css('width', '' + percent + '%')
        }
      }
    })
}
